//
//  ViewController.swift
//  CardGame1
//
//  Created by Robert Randell on 28/10/2014.
//  Copyright (c) 2014 The APS Group. All rights reserved.
//

import UIKit

struct Decks {
    var playerDeck: [trumpCard] = []
    var computerDeck: [trumpCard] = []
}

class ViewController: UIViewController {
    
    let cards = trumpCardValues()
    var decks = Decks()
    
    var playerValueOne: Int = 0
    var playerValueTwo: Int = 0
    var playerValueThree: Int = 0
    var playerValueFour: Int = 0
    
    var computerValueOne: Int = 0
    var computerValueTwo: Int = 0
    var computerValueThree: Int = 0
    var computerValueFour: Int = 0
    
    var playerCardIndex: Int = 0
    var computerCardIndex: Int = 0
    
    var cardObjectsArray: [trumpCard] = []
    
    @IBOutlet weak var optionAValue: UILabel!
    @IBOutlet weak var optionBValue: UILabel!
    @IBOutlet weak var optionCValue: UILabel!
    @IBOutlet weak var optionDValue: UILabel!
    
    @IBOutlet weak var computerOptAValue: UILabel!
    @IBOutlet weak var computerOptBValue: UILabel!
    @IBOutlet weak var computerOptCValue: UILabel!
    @IBOutlet weak var computerOptDValue: UILabel!
    
    @IBOutlet weak var playerCardTitle: UILabel!
    @IBOutlet weak var computerCardTitle: UILabel!
    
    @IBOutlet weak var playerCardCountContainer: UIView!
    @IBOutlet weak var computerCardCountContainer: UIView!
    
    @IBOutlet weak var coverCard: UIImageView!
    
    @IBOutlet weak var gameOverLabel: UILabel!
    @IBOutlet weak var gameOverBtn: UIButton!
    
    @IBOutlet weak var cardResultLabel: UILabel!
    @IBOutlet weak var cardResultBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.coverCard.alpha = 1.0
        
        for var i = 0; i < cards.trumpCardsValues.count; i++ {
            
            var valueOfId: Int = cards.trumpCardsValues[i]["ID"]!
            var valueOfOne: Int = cards.trumpCardsValues[i]["A"]!
            var valueOfTwo: Int = cards.trumpCardsValues[i]["B"]!
            var valueOfThree: Int = cards.trumpCardsValues[i]["C"]!
            var valueOfFour: Int = cards.trumpCardsValues[i]["D"]!
            
            var valueOfTitle: String = cards.trumpCardsTitles[i]
            
            var cardObject = trumpCard(idNumber: valueOfId, idTitle: valueOfTitle, optionOne: valueOfOne, optionTwo: valueOfTwo, optionThree: valueOfThree, optionFour: valueOfFour)
            self.cardObjectsArray.append(cardObject)
            
        }
        self.startGame()
    }
    
    // MARK: - Setup, Reset and Game Over Methods
    
    func startGame() {
        
        self.resetGame()
        
        var shuffledCardDeck: Array = self.shuffle(self.cardObjectsArray)
        self.splitCardDeck(shuffledCardDeck)
        self.showPlayerCard()
    }
    
    func resetGame() {
        
        self.gameOverLabel.alpha = 0.0
        self.gameOverBtn.alpha = 0.0
        self.gameOverBtn.enabled = false
        
        self.cardResultLabel.alpha = 0.0
        self.cardResultBtn.alpha = 0.0
        self.cardResultBtn.enabled = false
        
        self.coverCard.alpha = 1.0
        
        decks.playerDeck.removeAll(keepCapacity: false)
        decks.computerDeck.removeAll(keepCapacity: false)
    }
    
    func gameOver(result: Int) {
        
        self.gameOverLabel.alpha = 1.0
        self.gameOverBtn.alpha = 1.0
        self.gameOverBtn.enabled = true
        
        self.cardResultLabel.alpha = 0.0
        self.cardResultBtn.alpha = 0.0
        self.cardResultBtn.enabled = false
        
        self.coverCard.alpha = 0.0
        
        switch result {
        case 0:
            self.gameOverLabel.text = "Hard luck! The computer has won this round."
        default:
            self.gameOverLabel.text = "Awesome! You have won this round."
        }
    }
    
    func shuffle<T>(var shuffledCards: Array<T>) -> Array<T> {
        for i in 0..<(shuffledCards.count - 1) {
            let j = Int(arc4random_uniform(UInt32(shuffledCards.count - i))) + i
            swap(&shuffledCards[i], &shuffledCards[j])
        }
        return shuffledCards
    }
    
    func splitCardDeck(shuffledDeck: Array<trumpCard>) -> (playerDeck: Array<trumpCard>, computerDeck: Array<trumpCard>) {
        
        var cardSplit: Int = shuffledDeck.count / 2
        
        for i in 0..<cardSplit {
            decks.playerDeck.append(shuffledDeck[i])
        }
        for i in cardSplit..<shuffledDeck.count {
            decks.computerDeck.append(shuffledDeck[i])
        }
        return (decks.playerDeck, decks.computerDeck)
    }
    
    // MARK: - Game Loop
    
    func determineWinner(playerHasValueOf: Int, computerHasValueOf: Int) {
        
        self.cardResultLabel.alpha = 1.0
        self.cardResultBtn.alpha = 1.0
        self.cardResultBtn.enabled = true
        
        self.coverCard.alpha = 0.0
        
        if playerHasValueOf > computerHasValueOf {
            decks.playerDeck.append(decks.computerDeck[self.computerCardIndex])
            decks.computerDeck.removeAtIndex(self.computerCardIndex)
            
            self.cardResultLabel.backgroundColor = cards.fullBlueColour
            self.cardResultLabel.text = "You won as \(playerHasValueOf) is higher than \(computerHasValueOf)!"
            
            self.cardResultBtn.backgroundColor = cards.fullBlueColour
        }
        else {
            decks.computerDeck.append(decks.playerDeck[self.playerCardIndex])
            decks.playerDeck.removeAtIndex(self.playerCardIndex)
            
            self.cardResultLabel.backgroundColor = cards.fullRedColour
            self.cardResultLabel.text = "Sorry. The computer won as \(computerHasValueOf) is higher or equal to \(playerHasValueOf)!"
            
            self.cardResultBtn.backgroundColor = cards.fullRedColour
        }
    }
    
    func showPlayerCard() {
        
        self.coverCard.alpha = 1.0
        
        self.cardResultLabel.alpha = 0.0
        self.cardResultBtn.alpha = 0.0
        self.cardResultBtn.enabled = false
        
        if decks.playerDeck.count == 0 {
            self.gameOver(0)
        }
        else if decks.computerDeck.count == 0 {
            self.gameOver(1)
        }
        else {
        
            self.playerCardIndex = Int(arc4random_uniform(UInt32(decks.playerDeck.count)))
            self.computerCardIndex = Int(arc4random_uniform(UInt32(decks.computerDeck.count)))
            
            var playerCard = decks.playerDeck[playerCardIndex]
            var computerCard = decks.computerDeck[computerCardIndex]
            
            self.playerValueOne = playerCard.optionOne
            self.playerValueTwo = playerCard.optionTwo
            self.playerValueThree = playerCard.optionThree
            self.playerValueFour = playerCard.optionFour
            
            self.playerCardTitle.text = playerCard.idTitle
            self.optionAValue.text = (String(self.playerValueOne))
            self.optionBValue.text = (String(self.playerValueTwo))
            self.optionCValue.text = (String(self.playerValueThree))
            self.optionDValue.text = (String(self.playerValueFour))
            
            self.computerValueOne = computerCard.optionOne
            self.computerValueTwo = computerCard.optionTwo
            self.computerValueThree = computerCard.optionThree
            self.computerValueFour = computerCard.optionFour
            
            self.computerCardTitle.text = computerCard.idTitle
            self.computerOptAValue.text = (String(self.computerValueOne))
            self.computerOptBValue.text = (String(self.computerValueTwo))
            self.computerOptCValue.text = (String(self.computerValueThree))
            self.computerOptDValue.text = (String(self.computerValueFour))
            
            self.cardUpdateCountImages()
        }
    }
    
    func cardUpdateCountImages() {
        
        let playerSubviews: Array = self.playerCardCountContainer.subviews
        let computerSubviews: Array = self.computerCardCountContainer.subviews
        
        for (var view) in playerSubviews {
            view.removeFromSuperview()
        }
        
        for (var view) in computerSubviews {
            view.removeFromSuperview()
        }
        
        var cardCountImage: UIImageView
        let increment: CGFloat = 12
        
        for var i = 0; i < decks.playerDeck.count; i++ {
            
            var x: CGFloat = CGFloat(i)
            
            cardCountImage = UIImageView(frame:CGRectMake(increment * x, 0, 10, 32));
            cardCountImage.backgroundColor = cards.blueColour
            
            //cardCountImage.image = UIImage(named:"imageName.png")
            
            self.playerCardCountContainer.addSubview(cardCountImage)
        }
        
        for var i = 0; i < decks.computerDeck.count; i++ {
        
            var x: CGFloat = CGFloat(i)
            
            cardCountImage = UIImageView(frame:CGRectMake(increment * x, 0, 10, 32));
            cardCountImage.backgroundColor = cards.redColour
            
            self.computerCardCountContainer.addSubview(cardCountImage)
        }
    }
    
    // MARK: - Button Actions
    
    @IBAction func playAgain(sender: UIButton) {
        self.startGame()
    }
    
    @IBAction func nextCard(sender: UIButton) {
        self.showPlayerCard()
    }
    
    @IBAction func compareValues(sender: UIButton) {
        
        var playerOptionPicked: Int = sender.tag
        
        var playerCard: Int = self.playerCardIndex
        var computerCard: Int = self.computerCardIndex
        
        var playerValue: Int = 0
        var computerValue: Int = 0
        
        switch playerOptionPicked {
        case 1:
            playerValue = self.playerValueOne
            computerValue = self.computerValueOne
        case 2:
            playerValue = self.playerValueTwo
            computerValue = self.computerValueTwo
        case 3:
            playerValue = self.playerValueThree
            computerValue = self.computerValueThree
        default:
            playerValue = self.playerValueFour
            computerValue = self.computerValueFour
        }
        self.determineWinner(playerValue, computerHasValueOf: computerValue)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
