//
//  TrumpCard.swift
//  CardGame1
//
//  Created by Robert Randell on 28/10/2014.
//  Copyright (c) 2014 The APS Group. All rights reserved.
//

import Foundation

class trumpCard: NSObject {
    
    var idNumber: Int
    var optionOne: Int
    var optionTwo: Int
    var optionThree: Int
    var optionFour: Int
    
    var idTitle: String
    
    init(idNumber: Int, idTitle: String, optionOne: Int, optionTwo: Int, optionThree: Int, optionFour: Int) {
        
        self.idNumber = idNumber
        self.optionOne = optionOne
        self.optionTwo = optionTwo
        self.optionThree = optionThree
        self.optionFour = optionFour
        
        self.idTitle = idTitle
        
    }
    
    
}
