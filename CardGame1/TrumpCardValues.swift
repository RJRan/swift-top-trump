//
//  TrumpCardValues.swift
//  CardGame1
//
//  Created by Robert Randell on 28/10/2014.
//  Copyright (c) 2014 The APS Group. All rights reserved.
//

import Foundation
import UIKit

class trumpCardValues {
    
    // MARK: - Constants
    // Must have even number of cards...
    
    let trumpCardsValues = [
        ["ID" : 0, "A" : 1, "B" : 24, "C" : 67, "D" : 84],
        ["ID" : 1, "A" : 96, "B" : 5, "C" : 36, "D" : 19],
        ["ID" : 2, "A" : 55, "B" : 88, "C" : 11, "D" : 26],
        ["ID" : 3, "A" : 12, "B" : 42, "C" : 85, "D" : 74],
        ["ID" : 4, "A" : 32, "B" : 3, "C" : 59, "D" : 6],
        ["ID" : 5, "A" : 9, "B" : 23, "C" : 14, "D" : 67],
        ["ID" : 6, "A" : 68, "B" : 21, "C" : 46, "D" : 36],
        ["ID" : 7, "A" : 71, "B" : 70, "C" : 76, "D" : 67]
    ]
    
    let trumpCardsTitles = ["Card 1", "Card 2", "Card 3", "Card 4", "Card 5", "Card 6", "Card 7", "Card 8",]
    
    let fullBlueColour: UIColor = UIColor(red: 74.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
    let fullRedColour: UIColor = UIColor(red: 244.0/255.0, green: 96/255.0, blue: 96/255.0, alpha: 1.0)
    
    let blueColour: UIColor = UIColor(red: 74.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 0.5)
    let redColour: UIColor = UIColor(red: 244.0/255.0, green: 96/255.0, blue: 96/255.0, alpha: 0.5)
    
}

